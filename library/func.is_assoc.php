<?
function is_assoc($_array) { 
	// return array_keys($_array) !== range(0, count($_array) - 1);

    if ( !is_array($_array) || empty($_array) ) { 
        return -1; 
    } 
    foreach (array_keys($_array) as $k => $v) { 
        if ($k !== $v) { 
            return true; 
        } 
    } 
    return false; 
} 
<?php
/**
 * Funções genéricas
 */
class Func
{
	public function img($img)
	{
		return "<img src=\"".ROOT_PATH.$img."\" />";
	}
	
	public function removeDecorators(&$elm)
    {
        $elm->removeDecorator('label')
			->removeDecorator('htmlTag')
            ->removeDecorator('description')
            ->removeDecorator('errors');
    }
	
	public function clean($val)
	{
		return str_replace(array('.',',',' ','-','_','(',')','[',']'),'',trim($val));
	}
	
	public function br($j)
	{
		$br = "";
		for($i=0;$i<=$j;$i++){
			$br.= "<br/>";
		}
		return $br;
	}
    
    public static function _toUtf8(&$item, $key) {
        $item = iconv("iso-8859-1","utf-8",$item);
    }
    
    public static function _toIso(&$item, $key) {
        $item = iconv("utf-8","iso-8859-1",$item);
    }
    
    public function arrayToObject($array)
    {
        $object = new stdClass();
        if (is_array($array)){
            foreach ($array as $name=>$value){
                $name = trim($name);
                if (!empty($name)){
                    $object->$name = $value;
                }
            }
        } else {
            //die("param is not an array.");
        }
        return $object;
    }
    
    public function objectToArray($object)
    {
        $array = array();
        if (is_object($object)){
            $array = get_object_vars($object);
        }
        return $array;
    }
    
    public static function _encodeUtf8(&$item,$key)
	{
		$item = utf8_encode($item);
	}
    
    public static function _decodeUtf8(&$item,$key)
	{
		$item = utf8_decode($item);
	}
    
    public static function _arrayToObject(&$item,$key)
	{
		$item = self::arrayToObject($item);
	}
    
    public function drop($name=null,$belong,$options,$value='',$class='f1')
	{
		array_walk($options,'Func::_encodeUtf8');
		$drop = new Zend_Form_Element_Select($belong,$name ? array('belongsTo' => $name) : array());
		$drop->setAttrib('class',$class)
			->addMultiOptions($options)
			->setValue($value);
		Func::removeDecorators($drop);
		return $drop;
	}
    
    public function chk_cb($val1,$val2)
    {
        return $val1 == $val2 ? 'checked="checked"' : '';
    }
    
    public function chk_drop($val1,$val2)
    {
        return $val1 == $val2 ? 'selected="selected"' : '';
    }
}

class Img
{
    public function bg($path,$preload=false)
    {
        $path = URL."/img/".str_replace("/","|",$path);
        $style = "style=\"background-image:url('$path');\"";
        
        if($preload){
            $img = 'img_'.rand(0,999999);
            $script = Js::script("var $img = new Image(); $img.src = '$path';");
            return $style.'>'.substr($script,0,strlen($script)-2);
        }
        
        return $style;
    }
    
    public function src($path,$attrs="")
    {
        return "<img src='".URL."/img/".str_replace("/","|",$path)."' ".$attrs." />";
    }
}

class Image
{
    public function bg($path)
    {
        return "style=\"background-image:url('".CSS_PATH."/img/".$path."');\"";
    }
    
    public function src($path,$attrs="",$default_path=true)
    {
        $path = ($default_path ? IMG_PATH."/" : "").$path;
        return "<img src='".$path."' ".$attrs." />";
    }

    /**
     * Retorna cor predominante da imagem
     * 
     * @param string $thumb - resource de imagem: instância de PhpThumb -> irá gerar o resource através to PhpThumb transformando a imagem para tamanho 1x1
     *                        [OU] imagecreatetruecolor() || $thumbInstance->getWorkingImage() -> neste caso, a imagem precisa estar com tamanho 1x1 pois não irá ser transformada
     * 
     * @param string $return - tipo de retorno da função (hex, rgb)
     * 
     * @return string|array - se $return == hex: (string) cor em forma hexadecimal - "#000000"
     *                        se $return == rgb: (array)  cor em forma rgb - array(0,0,0)
     */
    public function getImageColor($thumb, $return='hex')
    {
        if(!is_resource($thumb) && get_class($thumb)=='GdThumb'){
            $scaled = $thumb;
            $scaled->adaptiveResize(1,1);
            $thumb = $scaled->getWorkingImage();
        }
        
        if(!is_resource($thumb)) return false;

        $meanColor = imagecolorat($thumb, 0, 0);
        $r = ($meanColor >> 16) & 0xFF;
        $g = ($meanColor >> 8) & 0xFF;
        $b = $meanColor & 0xFF;
        $rgb = array($r,$g,$b);
        
        return ($return=='rgb') ? $rgb : rgb2hex($rgb);
    }

    /**
     * Verifica cor predominante da imagem, checando se é escura
     * 
     * @param string $thumb - resource de imagem (imagecreatetruecolor() || $thumbInstance->getWorkingImage())
     *                        || instância de PhpThumb
     * 
     * @return bool - true: imagem é escura, false: imagem é clara
     */
    public function isDarkness($thumb)
    {
        $rgb = self::getImageColor($thumb,'rgb');
        $bright_avg = Is_Math::avg($rgb);
        $brightness = $bright_avg < 125;
        return $brightness;
    }

    /**
     * Seta resolução dpi da imagem
     * (* Necessita Imagick)
     * 
     * @param string $path - caminho da imagem
     * @param int    $h    - resolução horizontal
     * @param int    $v    - resolução vertical
     */
    public function setDpi($path,$h=72,$v=72)
    {
        if(extension_loaded('imagick')){
            $imagick = new Imagick();
            $imagick->readImage($path);
            
            // setando dpi
            $imagick->setImageResolution($h,$v);
            $imagick->resampleImage($h,$v,imagick::FILTER_UNDEFINED,0);
            
            $imagick->writeImage($path);
        }
    }

    /**
     * Seta qualidade da imagem
     * 
     * @param string $source_url      - caminho da imagem
     * @param string $destination_url - caminho da imagem de destino
     * @param int    $quality         - qualidade desejada (0~100)
     */
    public function setQuality($source_url, $destination_url, $quality)
    {
        $info = getimagesize($source_url);
     
        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
        elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
        else throw new Exception('Image format not supported');
     
        //save it
        imagejpeg($image, $destination_url, $quality);
     
        //return destination file url
        return $destination_url;
    }

    /*
     * Tratamento de imagem padrão
     * 
     * - redimensiona para um tamanho máximo
     * - aplica marca d'água
     * - seta dpi
     * 
     * @param string $path - caminho da imagem
     * @param array  $conf - configurações do tratamento (auto-explicativo) 
     *                       - setar atributo para false se não for aplicável
     */
    public function process($path, $_conf=array())
    {
        $conf = $_conf + array(
            'resize'    => array(2000,2000),
            'watermark' => false,
            'dpi'       => false, //array(72,72),
            'quality'   => false,
        );

        $thumb = Php_Thumb_Factory::create($path);
        // $thumb2 = Php_Thumb_Factory::create($path); // thumb auxiliar para verificar brilho
        
        // redimensionando para o máximo de largura x altura
        if((bool)@$conf['resize']) 
            $thumb->resize($conf['resize'][0],$conf['resize'][1]);
        
        if((bool)@$conf['watermark']){
            // seleciona marca clara/escura dependendo da cor predominante da imagem            
            $watermark = WATERMARK_PATH_LIGHT; //(Image::isDarkness($thumb2)) ? WATERMARK_PATH_LIGHT : WATERMARK_PATH_DARK;
            // aplicando marca d'água
            $thumb->createWatermark($watermark,'cc',0,$thumb);
        }
        
        $thumb->save($path); // precisamos salvar antes de aplicar o dpi
        
        // setando dpi máximo
        if((bool)@$conf['dpi']) 
            Image::setDpi($path,$conf['dpi'][0],$conf['dpi'][1]);
        
        // setando qualidade
        if((bool)@$conf['quality']) 
            Image::setQuality($path,$path,$conf['quality']);
    }
}

class Youtube
{
    public function checkStr($str)
    {
        $regex = "#youtu(be.com|.b)(/v/|/watch\\?v=|e/|/watch(.+)v=)(.{11})#";
        preg_match_all($regex , $str, $m);
        return $m;
    }
    
    public function hasVideo($str)
    {
        $s = self::checkStr($str);
        return count($s[0]) ? $s : false;
    }
    
    public function getText($str)
    {
        if(!$s = self::hasVideo($str)) return $str;
        $p = '/(.*)(http.*)/';
        preg_match($p,$str,$m);
        return $m[1];
    }
    
    public function getUrl($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[0][0];
    }
    
    public function getCode($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[4][0];
    }

    public function embedUrl($code,$params=null)
    {
        $url = 'http://www.youtube.com/embed/'.$code;
        if($params) $url.= '?'.$params;
        return $url;
    }

    public function embed($code,$width=640,$height=480)
    {
        $url = 'http://www.youtube.com/embed/'.$code.'?autohide=1';
        return '<iframe '.
                'width="'.$width.'" '.
                'height="'.$height.'" '.
                'src="'.$url.'" '.
                'frameborder="0" '.
                'allowfullscreen></iframe>';
    }

    /**
     * @param string $n - 0,1,2,3 | default | mqdefault | maxresdefault
     */
    public function thumbnail($url=null,$n='mqdefault')
    {
        // $queryString = parse_url($url ? $url : self::getUrl(), PHP_URL_QUERY);
        // parse_str($queryString, $params);
        // return "i3.ytimg.com/vi/{$params['v']}/default.jpg";
        return 'http://i3.ytimg.com/vi/'.self::getCode($url).'/'.$n.'.jpg';
    }
    
    public function parseStr($str,$params=array())
    {
        $o = new stdClass();
        $o->type = 'youtube';
        $o->text = self::getText($str);
        $o->url  = self::getUrl($str);
        $o->code = self::getCode($str);
        $o->embed = (isset($params['embed_width']) && isset($params['embed_height'])) ? 
                    self::embed($o->code,$params['embed_width'],$params['embed_height']) : 
                    self::embed($o->code);
        $o->embedUrl  = self::embedUrl($o->code);
        $o->thumbnail = self::thumbnail($o->url,(isset($params['thumbnail_youtube']) ? $params['thumbnail_youtube'] : 'mqdefault'));
        return $o;
    }
}

class Vimeo
{
    public function checkStr($str)
    {
        $regex = "#vimeo(.com|.b)(/)(\d{4,})#";
        preg_match_all($regex , $str, $m);
        return $m;
    }
    
    public function hasVideo($str)
    {
        $s = self::checkStr($str);
        return count($s[0]) ? $s : false;
    }
    
    public function getText($str)
    {
        if(!$s = self::hasVideo($str)) return $str;
        $p = '/(.*)(http.*)/';
        preg_match($p,$str,$m);
        return $m[1];
    }
    
    public function getUrl($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[0][0];
    }
    
    public function getCode($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[3][0];
    }

    public function embedUrl($code,$params=null)
    {
        $url = 'http://player.vimeo.com/video/'.$code;
        if($params) $url.= '?'.$params;
        return $url;
    }

    public function embed($code,$width=640,$height=480)
    {
        $url = 'http://player.vimeo.com/video/'.$code.'';
        return '<iframe '.
                'width="'.$width.'" '.
                'height="'.$height.'" '.
                'src="'.$url.'" '.
                'frameborder="0" '.
                'allowfullscreen></iframe>';
    }

    public function thumbnail($url=null,$n='thumbnail_medium')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://vimeo.com/api/v2/video/'.self::getCode($url).'.php');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $output = unserialize(curl_exec($ch));
        $output = $output[0];
        curl_close($ch);
        return $output[$n];
    }
    
    public function parseStr($str)
    {
        $o = new stdClass();
        $o->type = 'vimeo';
        $o->text = self::getText($str);
        $o->url  = self::getUrl($str);
        $o->code = self::getCode($str);
        $o->embed = (isset($params['embed_width']) && isset($params['embed_height'])) ? 
                    self::embed($o->code,$params['embed_width'],$params['embed_height']) : 
                    self::embed($o->code);
        $o->embedUrl  = self::embedUrl($o->code);
        $o->thumbnail = self::thumbnail($o->url,(isset($params['thumbnail_vimeo']) ? $params['thumbnail_vimeo'] : 'thumbnail_medium'));
        return $o;
    }
}

function _checkVideoStr($str,$params=array()){
    if(strstr($str,'vimeo.com')) return Vimeo::parseStr($str,$params);
    if(strstr($str,'youtube.com')) return Youtube::parseStr($str,$params);
    return false;
}

function _iframeVideo($url,$width=640,$height=480){
    if(strstr($url,'vimeo.com')) return Vimeo::embed(Vimeo::getCode($url),$width,$height);
    if(strstr($url,'youtube.com')) return Youtube::embed(Youtube::getCode($url),$width,$height);
    return false;
}

function _thumbVideo($url){
    if(strstr($url,'vimeo.com')) return Vimeo::thumbnail($url);
    if(strstr($url,'youtube.com')) return Youtube::thumbnail($url);
    return false;
}

function _thumbVideoImg($url,$alt='',$attrs='')
{
    $img = _thumbVideo($url);
    return ($img) ? '<img src="'.$img.'" alt="'.$alt.'" '.$attrs.'/>' : '';
}

function _d($var,$exit=true){ return Is_Var::dump($var,$exit); }
function _e($var,$exit=true){ return Is_Var::export($var,$exit); }

function export_url($section,$ext)
{
    $url = explode('/'.$section,'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    return implode('/',array($url[0],$section,'export.'.$ext.$url[1]));
}

function _currency($v,$view=null)
{
    return $view ? $view->currency($v) : 'R$ '.number_format($v,2,',','.');
}

function _currencyParcel($v,$parc,$view=null)
{
    return $view ? $view->currency($v/$parc) : 'R$ '.number_format($v/$parc,2,',','.');
}

function _utfRow($row){ return Is_Array::utf8DbRow($row); }

function _utfRows($rows){ return Is_Array::utf8DbResult($rows); }

function dtbr($dt,$sep='/',$withTime=false,$time_sep="h",$withSec=false){
    return $withTime ? 
        Is_Date::am2brWithTime($dt,$sep,$time_sep,$withSec):
        Is_Date::am2br($dt,$sep);
}
function dtam($dt,$sep='-',$withTime=false,$time_sep="h",$withSec=false){
    return $withTime ? 
        Is_Date::br2amWithTime($dt,$sep,$time_sep,$withSec):
        Is_Date::br2am($dt,$sep);
}

function cleanHtml($text){
    // $tags = '<b><strong><p><div><a><i><u><ul><ol><li><img><br><h1><h2><h3><h4><h5><h6><table><thead><tbody><th><tr><td><hr>';
    $tags = 'b,strong,p,div,a,i,u,ul,ol,li,img,br,h1,h2,h3,h4,h5,h6,'.
            'table,thead,tbody,th,tr,td,hr,strike';
    $tags = '<'.implode('><',explode(',',$tags)).'>';
    $text = stripslashes(($text));
    $text = (strip_tags($text,$tags));
    
    // replace contents
    $replaces = array(
        //'\<a(.*)\<\/a\>' => '<a target="_blank" rel="nofollow" \\1</a>',
        // '<([a-zA-Z0-9]*)(.*)(/?)(.*)>' => '<\\1 \\3>',
        // 'style\=\"(.*)\"' => ' ',
    );
    foreach($replaces as $p => $r) $text = ereg_replace($p,$r,$text);

    // import html
    Lib::import('phpQuery');
    $html = phpQuery::newDocumentHTML($text);

    // add attributes
    $adds = array(
        'a' => array('target'=>'_blank','rel'=>'nofollow')
    );
    foreach($adds as $p => $r) foreach($r as $k => $v) $html->find($p)->attr($k,$v);

    // clean styles
    $allow_styles = array(
        'text-align: center',
        'text-align: left',
        'text-align: right',
        'text-align: justify',
        'width: 100%'
    );
    
    foreach (explode('><',substr($tags,1,-1)) as $tag) {
        $items = $html->find($tag);
        
        foreach ($items as $item) {
            $style = array();

            foreach ($allow_styles as $as) {
                if(strstr(pq($item)->attr('style'), $as)) $style[] = $as;
            }

            (count($style)) ? 
                pq($item)->attr('style',implode(';',$style)) : 
                pq($item)->removeAttr('style');

            pq($item)->removeAttr('face');
        }
    }
    
    return $html->html();
    // return $text;
}

function cleanHtml1($text){
    // $tags = '<b><strong><p><div><a><i><u><ul><ol><li><img><br><h1><h2><h3><h4><h5><h6><table><thead><tbody><th><tr><td><hr>';
    $tags = 'b,strong,p,div,a,i,u,ul,ol,li,img,br,h1,h2,h3,h4,h5,h6,'.
            'table,thead,tbody,th,tr,td,hr,strike';
    $tags = '<'.implode('><',explode(',',$tags)).'>';
    $text = stripslashes(($text));
    $text = (strip_tags($text,$tags));
    
    // replace contents
    $replaces = array(
        //'\<a(.*)\<\/a\>' => '<a target="_blank" rel="nofollow" \\1</a>',
        // '<([a-zA-Z0-9]*)(.*)(/?)(.*)>' => '<\\1 \\3>',
        // 'style\=\"(.*)\"' => ' ',
    );
    foreach($replaces as $p => $r) $text = ereg_replace($p,$r,$text);

    // import html
    Lib::import('phpQuery');
    $html = phpQuery::newDocumentHTML($text);

    // add attributes
    $adds = array(
        'a' => array('target'=>'_blank','rel'=>'nofollow')
    );
    foreach($adds as $p => $r) foreach($r as $k => $v) $html->find($p)->attr($k,$v);

    // clean styles
    $allow_styles = array(
        'text-align: center',
        'text-align: left',
        'text-align: right',
        'text-align: justify',
        'width: 100%'
    );
    
    foreach (explode('><',substr($tags,1,-1)) as $tag) {
        $items = $html->find($tag);
        
        foreach ($items as $item) {
            $style = array();

            foreach ($allow_styles as $as) {
                if(strstr(pq($item)->attr('style'), $as)) $style[] = $as;
            }

            (count($style)) ? 
                pq($item)->attr('style',implode(';',$style)) : 
                pq($item)->removeAttr('style');

            pq($item)->removeAttr('face');
        }
    }
    
    return $html->html();
    // return $text;
}

function _parseEndereco($dados)
{
    $endereco = $dados->endereco_pt;
    $bairro = $dados->bairro_pt;
    $cidade = $dados->cidade;
    $estado = $dados->estado;

    $endereco = reset(explode(' cj.', $endereco));
    $endereco = reset(explode(' cj ', $endereco));
    $endereco = reset(explode(' conj.', $endereco));
    $endereco = reset(explode(' conj ', $endereco));
    $endereco = reset(explode(' Cj.', $endereco));
    $endereco = reset(explode(' Cj ', $endereco));
    $endereco = reset(explode(' Conj.', $endereco));
    $endereco = reset(explode(' Conj ', $endereco));
    $endereco = reset(explode(' - ', $endereco));
    $bairro = reset(explode(' - ', $bairro));
    
    return implode(', ',array($endereco,$bairro,$cidade,$estado));
}

function pedidoDataHora($data)
{
    if(!(bool)trim(@$data)) return array('','','');
    
    $pedido_datahora = $data;
    $p_datahora = explode(' ',$pedido_datahora);
    $p_data = Is_Date::am2br($p_datahora[0]);
    $p_hora = substr($p_datahora[1],0,-3);
    $p_datahora = $p_data.' '.$p_hora;

    return array($p_data,$p_hora,$p_datahora);
}

function pedidoFormaEntrega($taxas)
{
    $forma = '';

    foreach($taxas as $taxa)
        if(strstr($taxa->descricao,'orreio'))
            $forma = trim(end(explode('-',$taxa->descricao)));

    return $forma;
}

/**
 * Retorna URL da página para ser usado em templates, email, etc.
 * (não é usada a define URL para ser a mesma em ambos os ambientes)
 */
function site_url($withHttp=true)
{
    return ($withHttp ? (IS_SSL ? 'https://' : 'http://') : '').'www.'.SITE_NAME.'.com.br';
}

function site_link($anchor=null, $attrs='')
{
    return '<a href="'.site_url().'" '.$attrs.'>'.($anchor ? $anchor : site_url(false)).'</a>';
}

/**
 * Auxliares para emails
 */
function mail_rodape()
{
    return '<br>'.
           '<p>Em caso de dúvidas, por favor entre em contato através de nosso email <a href="mailto:contato@'.SITE_NAME.'.com.br">contato@'.SITE_NAME.'.com.br</a>.<br><br>'.
           'Horário de atendimento, de segunda à sexta das 10:00 as 16:00.</p><br>'.
           '<p>Atenciosamente,<br>'.SITE_TITLE.'<br>'.site_link().'</p>';
}

function midiaImg($row)
{
    $img = IMG_URL.'/not-found.jpg';
    $img = IMG_URL.'/not-found.jpg';
    if($row->tipo_row=='video') $img = _thumbVideo($row->video);
    if($row->tipo_row=='clipping') $img = midiaImgClip($row);
    return $img;
}
function midiaImgClip($row,$fullurl=1)
{
    $img = null;
    if((bool)$row->capa) $img = $row->capa->path;
    else if((bool)$row->fotos) $img = $row->fotos[0]->path;
    else if((bool)trim($row->thumbnail)) $img = $row->thumbnail;
    if($fullurl) $img = IMG_URL.($img ? '/clippings/'.$img : '/not-found.jpg');
    return $img;
}
function midiaUrl($row,$urlpos=null)
{
    $url = '#';
    if($row->tipo_row=='video') $url = _checkVideoStr($row->video)->embedUrl;
    if($row->tipo_row=='clipping') $url = midiaImgClip($row);
    return $url;
}
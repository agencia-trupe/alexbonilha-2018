console.log('projetos');

var last_i = Number($('.projetos-lista .projeto').last().data('i'));

$('.load-projetos').click(function(e){
	e.preventDefault();

	var $this = $(this),
		last_page = $this.data('page'),
		url = [URL,CONTROLLER,'index.json?page='+last_page].join('/'),
		cat = $this.data('categoria'),
		i = (last_i==3) ? 0 : last_i, 
		row, j, html='', cssclass='', css='', img='';

	if(cat) url = url+'&categoria_id='+cat;
	// console.log(url); return;
	
	$.getJSON(url,function(json){
		if(json.error){
			alert(json.error);
			return false;
		}
		console.log(json.rows);

		for(j in json.rows){
			console.log(row);
			i++;
			row = json.rows[j];

			if(i==1) cssclass = 'projeto-1';
			if(i==2) cssclass = 'projeto-2';
			if(i==3) cssclass = 'projeto-3';
			img = URL+'/img/projetos:'+row.thumbnail;
			css = 'style=background-image:url('+img+')';
			
			html+= '<a href="'+URL+'/projeto/'+row.alias+'/'+row.id+'" class="projeto '+cssclass+'" '+css+' data-i="'+i+'">';
			html+= '<span class="img"><img src="'+img+'" alt=""></span>';
			html+= '<div class="overlay">';
			html+= '<p class="titulo">'+row['titulo_'+LANG]+'</p>';
			html+= '<span class="local">';
			html+= row['cidade_'+LANG]+' - '+row['uf_'+LANG];
			html+= '</span>';
			html+= '</div>';
			html+= '</a>';

			if(i==3) i = 0;
		}

		$('.projetos-lista').append(html);
		$this.data('page',Number(last_page)+1);
	});
	
	return false;
});
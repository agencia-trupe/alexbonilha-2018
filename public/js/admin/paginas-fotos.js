/*global $ */
$(document).ready(function () {
    $('#file_upload').fileUploadUI({
        uploadTable: $('#file_upload_list'),
        downloadTable: $('#paginas_fotos'),
        buildUploadRow: function (files, index) {
            //alert(files[index].size);
			//return $('<tr><td class="filename">' + files[index].name + '<\/td>' +
            return $('<tr>'+
					//'<td class="filename file_upload_preview"><\/td>' +
					'<td class="file_upload_progress"><div></div></td>' +
					'<td class="filesize">'+Files.formatBytes(files[index].size)+'</td>' +
                    '<td class="file_upload_cancel">' +
                    '<button class="ui-state-default ui-corner-all" title="Cancel">' +
                    '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
                    '<\/button>' +
					'<\/td>' +
					//'<td class="file_delete">' +
					//'<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
					//'<span class="ui-icon ui-icon-trash"><\/span>' +
					//'<\/button>' +
					//'<\/td>' +
					'<\/tr>');
        },
        buildDownloadRow: function (file) {
            //return $('<tr><td>' + file.name + '<\/td><\/tr>');
            if(file.error){
				return $('<tr><td><span>' + file.error + '<\/span><\/td><\/tr>');
			} else {
				return $('<li><a href=\'' + URL+'/img/'+DIR+":"+file.name + '\' rel=\'gallery_group\'>'+
						 '<img src=\'' + URL+'/img/'+DIR+":"+file.name + '?w=90&h=90\'><\/a>'+
						 '<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
						 '<span class="ui-icon ui-icon-trash"><\/span>' +
						 '<\/button>' +
						 '<button title="Inserir esta imagem no texto" class="ui-state-default ui-corner-all file-insert">' +
						 '<span class="ui-icon ui-icon-arrowthick-1-w"><\/span>' +
						 '<\/button>' +
						 '<input type="hidden" name="foto_id[]" value="'+file.id+'" class="foto_id" />' +
						 '<\/li>');
			}
        },
		beforeSend: function(event, files, index, xhr, handler, callBack){
			var patt_img  = /.*(gif|jpeg|png|jpg)$/;///(jpeg|jpg|png|gif|bmp)/gi,
				file_name = files[index].name,
				max_size  = 2213814;
			
			if(patt_img.test(Files.getExt(file_name)) && files[index].size < max_size){
				callBack();
			} else {
				var msg = !patt_img.test(Files.getExt(file_name)) ?
						  "Imagem inválida" :
						  "Tamanho máx. permitido: "+Files.formatBytes(max_size);
				
				handler.cancelUpload(event, files, index, xhr, handler);
				handler.addNode($('#file_upload_list'),
					$('<tr><td colspan="4">'+msg+' ('+file_name+')</td></tr>')
				);
				//callBack();
			}
		},
		onComplete: function (event, files, index, xhr, handler) {
			// var json = handler.response;
			Gallery.show();
		}
    });
    
    $("button.file-delete").live('click',function(){
        var row = $(this).parents("li");
		Files.del(row);
    });
    $("button.file-insert").live('click',function(){
        var row = $(this).parents("li");
		Files.insert(row);
    });
    
    $("#files tr").mouseover(function(){
        $(this).addClass("hover");
    }).mouseout(function(){
        $(this).removeClass("hover");
    });
});

Files = {
    formatBytes: function(size){
        var units = new Array(' B', ' KB', ' MB', ' GB', ' TB');
        for (var i = 0; size >= 1024 && i < 4; i++) size /= 1024;
        return Math.round(size)+units[i];
    },
    
    del: function(row,dir){
        dir = dir || false;
        var url = URL+"/admin/"+DIR+"/fotos-del.json?file="+row.find("input.foto_id").val();
        
        if(confirm("Deseja deletar a foto selecionada?")){
            $.getJSON(url,function(data){
                if(data.error){
                    alert(data.error);
                } else {
                    row.fadeOut("slow",function(){ $(this).remove(); });
                }
            });
        }
    },
	
	insert: function(row){
		var path = row.find('a').attr('href');
		$('textarea.wysiwyg').wysiwyg('insertImage', path);
	},
	
	getExt: function(name){
		return name.split(".").reverse()[0];
	}
}

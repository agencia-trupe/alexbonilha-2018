/*global $ */
var MsgRow = Messenger;
//MsgRow.container = $(".row-tools");
MsgRow.elm       = ".row-tools .row-status";

$(document).ready(function(){
    ajust_order();
    
    // titulo
    $('.frm-rodape-titulo').submit(function(e){
        e.preventDefault();
        
        var $form = $(this),
            form  = this,
            url   = form.action;
        
        $('.status_titulo').removeClass('error');
        Status.elm('.status_titulo').msg('Salvando...');
        $form.find('.bt').attr('disabled',true);
        
        $.post(url+'.json',$form.serialize(),function(json){
            if(json.error) Status.error(json.error);
            
            Status.msg(json.msg,5000);
        },'json');
        
        return false;
    });
    $('.status_titulo').live('click',function(){ $(this).fadeOut(); });
    
    
    // itens
    $('#rodape_items .row-list .row-order').live('click',function(){
        if(!$(this).hasClass('disabled')){
            var row = $(this).parent();
            if($(this).hasClass('order-down')){
                row.insertAfter(row.next());
            }else{
                if(row.prev().attr("class") != "title"){
                    row.insertBefore(row.prev());
                }
            }
            ajust_order();
        }
    });
    
    $('#rodape_items .row-list').sortable({
        revert:true,
        start:function(e,ui){
            $('#rodape_items .row-list li').addClass('disabled');
            ui.item.removeClass('disabled').addClass('grabber');
        },
        stop:function(e,ui){
            $('#rodape_items .row-list li').removeClass('disabled').removeClass('grabber');
            ajust_order();
        }
    });
    $('#rodape_items .row-list li.title').draggable({revert:'invalid'});
    //$('#rodape_items .row-list li').disableSelection();
    
    $('.row-status_id').live('click',function(){
        var row = $(this).parent();
        row.find('input[name="status_id[]"]').val(($(this).is(':checked')?"1":"0"));
    });
	
	$(".row-add").click(function(){
		Rows.add();
	});
	$(".row-action-del").live('click',function(){
		Rows.del($(this).parent());
	});
    $(".frm-rodape-items .bt").click(function(){
        //Rows.save();
    });
});

function ajust_order(){
    $('#rodape_items .row-list .row-ordem').each(function(i,v){
        $(this).val(i+1);
    });
    
    $('#rodape_items .row-list .row-order').removeClass('disabled');
    $('#rodape_items .row-list .order-up:first,#rodape_items .row-list .order-down:last').addClass('disabled');
}

Rows = {
	add: function(){
		var newRow = $("#tmplRow").tmpl().prependTo(".row-list");
		//newRow.find('input').val("");
		newRow.find('.row-descricao').focus();
        ajust_order();
	},
	
	del: function(row){
		if(confirm("Deseja remover o registro?")){
			var id        = row.find(".row-id");
			
			if($.trim(id.val()).length > 0){
				var url = URL+"/admin/home/del-items.json?id="+id.val();
				$.getJSON(url,function(data){
					if(data.error){
						alert(data.error);
					} else {
						Rows.remove(row);
					}
				});
			} else {
				Rows.remove(row);
			}
		}
	},
	
	remove: function(row){
		row.remove();
		MsgRow.add("Registro removido.").show(null,3000);
        ajust_order();
	},
	
	save: function(){
		$('.frm-categorias').submit();
	}
}

Status = {
    _elm : '#status',
    
    elm : function(sel){
        Status._elm = sel;
        return Status;
    },
    
    msg : function(m,delay,style){
        delay = delay || false;
        style = style || false;
        
        var elm = $(Status._elm);
        
        if(style) elm.addClass(style);
        
        elm.html(m);
        
        if(delay) elm.delay(delay).fadeOut('fast');
        
        return Status;
    },
    
    error : function(m,delay){
        delay = delay || false;
        return Status.msg(m,delay,'error');
    }
}
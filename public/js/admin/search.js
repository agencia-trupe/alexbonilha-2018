$(document).ready(function(){
    $(".search-result table tbody tr").mouseenter(function(){
        $(this).addClass("over");
    }).mouseleave(function(){
        $(this).removeClass("over");
    });
    
    $(".search-result table tbody tr td a.del").click(function(e){
        var elm = $(this);
        Msg.reset();
        
        if(confirm("Deseja realmente excluir o registro?")){
            e.preventDefault();
            
            var url = $(this).attr("href").replace("del","del.json");
            $.getJSON(url,function(data){
                if(data.erro){
                    Msg.add(data.erro,"erro").show();
                } else {
                    Msg.add("Registro excluído com sucesso.","message").show();
                    elm.parents("tr").fadeOut("slow",function(){ $(this).remove(); });
                }
            });
        }
        return false;
    });

    // ordenação
    if($('.row-order').length){
        ajust_order(true);
        
        $('.search-result table .row-order').live('click',function(){
            if(!$(this).hasClass('disabled')){
                var row = $(this).parent('td').parent('tr');
                if($(this).hasClass('order-down')){
                    row.insertAfter(row.next());
                }else{
                    if(row.prev().attr("class") != "title"){
                        row.insertBefore(row.prev());
                    }
                }
                ajust_order();
            }
        });
        
        $('.search-result table tbody').sortable({
            items: "tr",
            revert:true,
            start:function(e,ui){
                $('.search-result table tbody tr').addClass('disabled');
                ui.item.removeClass('disabled').addClass('grabber');
            },
            stop:function(e,ui){
                $('.search-result table tbody tr').removeClass('disabled').removeClass('grabber');
                ajust_order();
            }
        });
        // $('#'+CONTROLLER+' .row-list li.title').draggable({revert:'invalid'});
        // $('#'+CONTROLLER+' .row-list li.title').disableSelection();
    }
});

// ajusta ordem da lista e habilita/desabilita botões de ordenação
function ajust_order(noupdate){
    var values = {'id':[],'ordem':[]},
        update = !noupdate;

    $('.search-result table .row-ordem').each(function(i,v){
        var $this = $(this);
        $this.val(i+1);

        values.id.push($this.parent('td').find('.row-id').val());
        values.ordem.push(i+1);
    });

    if(update) $.post(URL+'/admin/'+CONTROLLER+'/ordem.json',values,function(json){
        if(json.error) alert(json.error);
    },'json');
    
    $('.search-result table .row-order').removeClass('disabled');
    $('.search-result table .order-up:first,.search-result table .order-down:last').addClass('disabled');
}
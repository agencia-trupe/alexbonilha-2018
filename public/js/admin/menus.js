/*global $ */
var MsgRow = Messenger;
//MsgRow.container = $(".row-tools");
MsgRow.elm       = ".row-tools .row-status";

$(document).ready(function(){
    ajust_order();
    
	// itens
    $('#rodape_items .row-list .row-order,#rodape_items2 .row-list .row-order').live('click',function(){
        if(!$(this).hasClass('disabled')){
            var row = $(this).parent();
            if($(this).hasClass('order-down')){
                row.insertAfter(row.next());
            }else{
                if(row.prev().attr("class") != "title"){
                    row.insertBefore(row.prev());
                }
            }
            ajust_order();
        }
    });
    
    $('#rodape_items .row-list,#rodape_items2 .row-list').sortable({
        revert:true,
        start:function(e,ui){
            $('#rodape_items .row-list li').addClass('disabled');
            ui.item.removeClass('disabled').addClass('grabber');
        },
        stop:function(e,ui){
            $('#rodape_items .row-list li').removeClass('disabled').removeClass('grabber');
            ajust_order();
        }
    });
	
	$(".row-add").click(function(){
		Rows.add();
	});
	$(".row-action-del").live('click',function(){
		Rows.del($(this).parent());
	});
    $(".frm-rodape-items .bt").click(function(){
        //Rows.save();
    });
	
	$('.row-action-sub').live('click',function(){
		$('#rodape_items2 .form-list').hide();
		$('#rodape_item_'+$(this).data('id')).fadeIn('fast');
	});
	
	$('#rodape_items2 a.cancel').live('click',function(){
		$('#rodape_item_'+$(this).data('id')).fadeOut('slow');
	});
});

function ajust_order(){
    $('#rodape_items .row-list .row-ordem').each(function(i,v){
        $(this).val(i+1);
    });
    
    $('#rodape_items .row-list .row-order').removeClass('disabled');
    $('#rodape_items .row-list .order-up:first,#rodape_items .row-list .order-down:last').addClass('disabled');
	
	// ajust2
    $('#rodape_items2 .form-list:visible .row-list .row-ordem').each(function(i,v){
        $(this).val(i+1);
    });
    
    $('#rodape_items2 .row-list .row-order').removeClass('disabled');
    $('#rodape_items2 .row-list .order-up:first,#rodape_items2 .row-list .order-down:last').addClass('disabled');
}

Rows = {
	add: function(){
		var newRow = $("#tmplRow").tmpl().prependTo(".row-list");
		//newRow.find('input').val("");
		newRow.find('.row-descricao').focus();
        ajust_order();
	},
	
	del: function(row){
		if(confirm("Deseja remover o registro?")){
			var id        = row.find(".row-id");
			
			if($.trim(id.val()).length > 0){
				var url = URL+"/admin/home/del-items.json?id="+id.val();
				$.getJSON(url,function(data){
					if(data.error){
						alert(data.error);
					} else {
						Rows.remove(row);
					}
				});
			} else {
				Rows.remove(row);
			}
		}
	},
	
	remove: function(row){
		row.remove();
		MsgRow.add("Registro removido.").show(null,3000);
        ajust_order();
	},
	
	save: function(){
		$('.frm-categorias').submit();
	}
}

Status = {
    _elm : '#status',
    
    elm : function(sel){
        Status._elm = sel;
        return Status;
    },
    
    msg : function(m,delay,style){
        delay = delay || false;
        style = style || false;
        
        var elm = $(Status._elm);
        
        if(style) elm.addClass(style);
        
        elm.html(m);
        
        if(delay) elm.delay(delay).fadeOut('fast');
        
        return Status;
    },
    
    error : function(m,delay){
        delay = delay || false;
        return Status.msg(m,delay,'error');
    }
}
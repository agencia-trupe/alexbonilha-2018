/*global $ */
var MsgLinha = Messenger;
//MsgLinha.container = $(".linha-tools");
MsgLinha.elm       = ".linha-tools .linha-status";

$(document).ready(function () {
	$(".linha-list li").live("mouseover",function(){
		$(this).find("a.linha-action").show();
	}).live("mouseout",function(){
		$(this).find("a.linha-action").hide();
	});
	
	$(".linha-add").click(function(){
		// Linha.add();
	});
	$(".linha-action-del").live('click',function(){
		Linha.del($(this).parent());
	});
	$(".linha-action-save").live('click',function(){
		Linha.save($(this).parent());
	});

	Foto.hideNewLinks($('ul.linha-list'));
	$('.foto-container').each(function(){
		var $this = $(this),
			$img  = $this.find('img');
		
		if($img.attr('src').indexOf('not-found.jpg') != -1){
			Foto.showNewLinks($this);
		}
	});
});

Foto = {
	showNewLinks : function(o){
		o.find('.row-foto-edit,.row-foto-del').hide();
		o.find('.row-foto-new').show();
		//o.find('.row-foto').removeClass('fbimg');
	},
	hideNewLinks : function(o){
		o.find('.row-foto-new').hide();
		o.find('.row-foto-edit,.row-foto-del').show();
		o.find('.row-foto').addClass('fbimg');
		$('.fbimg').fancybox();
	}
}

Linha = {
	add: function(){
		var newRow = $("#tmplLinha").tmpl({}).appendTo(".linha-list");
		$("input.mask-year").mask("9999");
		newRow.find('input.linha-ano').focus();
		Foto.showNewLinks(newRow);
	},
	
	del: function(row){
		if(confirm("Deseja remover o texto?")){
			var id        = row.find(".linha-id");
			
			if($.trim(id.val()).length > 0){
				var url = URL+"/admin/ideacao-passos/del.json?id="+id.val();
				$.getJSON(url,function(data){
					if(data.error){
						alert(data.error);
					} else {
						Linha.remove(row);
					}
				});
			} else {
				Linha.remove(row);
			}
		}
	},
	
	remove: function(row){
		row.remove();
		MsgLinha.add("Texto removido.").show(null,3000);
	},
	
	save: function(row){
		MsgLinha.add("Enviando...").show();
		
		var id          = row.find(".linha-id"),
			foto        = row.find(".foto_id"),
			foto_o      = row.find(".foto_id-original"),
			descricao   = row.find(".linha-descricao");
			descricao_o = row.find(".linha-descricao-original");
			
		var url = URL+"/admin/"+DIR+"/destaque-save.json?id="+id.val()
				+"&foto="+foto.val()+"&descricao="+urlencode(descricao.val())+"&pagina_id="+$("#linha-pagina-id").val();
		
		foto.attr('disabled',true);
		descricao.attr('disabled',true);
		
		$.getJSON(url,function(data){
			if(data.error){
				alert(data.error);
			} else {
				foto_o.val(foto.val());
				descricao_o.val(descricao.val());
				MsgLinha.add("Linha salva com sucesso.").show(null,3000);
				foto.attr('disabled',false);
				descricao.attr('disabled',false);

				Foto.showNewLinks(row);
			}
		});
	}
}

Files = {
    formatBytes: function(size){
        var units = new Array(' B', ' KB', ' MB', ' GB', ' TB');
        for (var i = 0; size >= 1024 && i < 4; i++) size /= 1024;
        return Math.round(size)+units[i];
    },
    
    del: function(row,dir){
        dir = dir || false;
        var url = URL+"/admin/"+DIR+"/fotos-del.json?file="+row.find("input.foto_id").val();
        
        if(confirm("Deseja deletar a foto selecionada?")){
            $.getJSON(url,function(data){
                if(data.error){
                    alert(data.error);
                } else {
                    row.find('.foto_id').val('0');
					row.find('.row-foto').attr('href',URL+'/img/not-found.jpg');
					row.find('img').attr('src',URL+'/img/not-found.jpg?w=20&h=20');
					Foto.showNewLinks(row);
                }
            });
        }
    },
	
	insert: function(row){
		var path = row.find('a').attr('href');
		$('textarea.wysiwyg').wysiwyg('insertImage', path);
	},
	
	getExt: function(name){
		return name.split(".").reverse()[0];
	}
}
var MAX_FOTOS = 0, MAX_SIZE = 5242880, allow_photos=false;

$(document).ready(function(){
    var _name = typeof(_NAME)=='undefined' ? false : _NAME;
    
    if(in_main('banners_'+ACTION) && _name == 'home'){
        sh_titulo();
        
        var configSelector = '.banner-configs input.txt'+
                            ',.banner-configs textarea.txt';
        
        $(configSelector).keyup(function(e){
            var $t = $(this), v = $.trim(nl2br($t.val()));
            
            $('.banner .preview.'+$t.attr('name')).html(v);
            sh_titulo();
        });
        
        // upload
        $('#file_upload_home').fileUploadUI({
            uploadTable: $('#file_upload_list'),
            //downloadTable: $('.list_fotos'),
            buildUploadRow: function (files, index) {
                return $('<tr>'+
                    '<td class="file_upload_progress"><div></div></td>' +
                    '<td class="filesize">'+Files.formatBytes(files[index].size)+'</td>' +
                    '<td class="file_upload_cancel">' +
                    '<button class="ui-state-default ui-corner-all" title="Cancel">' +
                    '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
                    '<\/button>' +
                    '<\/td>' +
                    '<\/tr>');
            },
            buildDownloadRow: function (file) {
                //return $('<tr><td>' + file.name + '<\/td><\/tr>');
                //alert(file.name);return;
                if(file.error){
                    return $('<tr><td><span>' + file.error + '<\/span><\/td><\/tr>');
                } else {
                    var file_path = URL+'/img/'+DIR+":"+file.name+'?w=740&h=514';
                    console.log(file_path);
                    $('.banner').css('background-image','url('+file_path+')');
                    return;
                }
            },
            beforeSend: function(event, files, index, xhr, handler, callBack){
                var _bg = $.trim($('.banner').css('background-image')),
                    _b = Boolean(_bg.length > 0 && _bg != 'none');
                
                var patt_img  = /.*(gif|jpeg|png|jpg)$/i,///(jpeg|jpg|png|gif|bmp)/gi,
                    file_name = files[index].name,
                    max_size  = (MAX_SIZE)  ? MAX_SIZE  : 2097152,
                    max_files = (MAX_FOTOS) ? MAX_FOTOS : 0,
                    file_count= _b ? 1 : 0,
                    max_count = (max_files>0) ? file_count < max_files : true,
                    msg       = "Erro ao enviar imagem";
                if(patt_img.test(Files.getExt(file_name)) && files[index].size < max_size && max_count){
                    callBack();
                    return true;
                }
                
                msg = !patt_img.test(Files.getExt(file_name)) ? "Imagem inválida" : msg;
                msg = files[index].size >= max_size ? "Tamanho máx. permitido: "+Files.formatBytes(max_size) : msg;
                msg = !max_count ? "Você pode enviar no máximo "+max_files+" fotos" : msg;
                
                handler.cancelUpload(event, files, index, xhr, handler);
                handler.addNode($('#file_upload_list'),
                    $('<tr class="error"><td colspan="3">'+msg+' ('+file_name+')</td></tr>')
                );
            },
            onComplete: function (event, files, index, xhr, handler) {
                // var json = handler.response;
                //Gallery.show();
            },
            onError: function (event, files, index, xhr, handler) {
                //console.log(files[0].name);return;
                if(xhr.responseXML == null){
                    handler.addNode($('#file_upload_list'),
                        $('<tr class="error"><td colspan="3">O servidor não respondeu a solicitação ('+files[0].name+')</td></tr>')
                    );
                }
            }
        });
        
        $("#file_upload_list tr.error").live('click',function(){
            $this = $(this);
            
            $this.fadeOut("fast",function(){
                $this.remove();
            })
        });
    }
});

function sh_titulo(){
    var v = $('.banner .preview.titulo');
    return ($.trim(v.text()) == '') ? v.hide() : v.show();
}

Files = {
    formatBytes: function(size){
        var units = new Array(' B', ' KB', ' MB', ' GB', ' TB');
        for (var i = 0; size >= 1024 && i < 4; i++) size /= 1024;
        return Math.round(size)+units[i];
    },
    
    del: function(row,dir){
        dir = dir || false;
        var url = URL+"/admin/"+DIR+"/fotos-del.json?file="+row.find("input.foto_id").val();
        
        if(confirm("Deseja deletar a foto selecionada?")){
            $.getJSON(url,function(data){
                if(data.error){
                    alert(data.error);
                } else {
                    row.fadeOut("slow",function(){ $(this).remove(); });
                }
            });
        }
    },
	
	insert: function(row){
		var path = row.find('a').attr('href');
		$('textarea.wysiwyg').wysiwyg('insertImage', path);
	},
	
	getExt: function(name){
		return name.split(".").reverse()[0];
	}
}
(function ($) {
    $.fn.isSimpleTabsLang = function(){
        var langs = SITE_LANGUAGES.split(','), langsSelector = [], langsSelector2 = [], i;

        for(i in langs) langsSelector.push('#form-new li[id$=_'+langs[i]+']');
        for(i in langs) langsSelector2.push('.tab-lang.tab-lang-'+langs[i]+'');
        
        var tabContainers = $(langsSelector.join(',')),
            tabContainers2 = $(langsSelector2.join(',')),
            hasHash = (window.location.hash && $('div.tabs-lang ul.tabs-navigation a[href="'+window.location.hash+'"]').size())?
                        window.location.hash : false,
            $tabsLinks = $('div.tabs-lang ul.tabs-navigation a');
        
        $tabsLinks.click(function () {
            var hash = this.hash.replace('#',''),
                filter = '[id$=_'+hash+']',
                filter2 = '.tab-lang.tab-lang-'+hash;
            tabContainers.hide().filter(filter).show();
            tabContainers2.hide().filter(filter2).show();
            
            $tabsLinks.removeClass('selected');
            $(this).addClass('selected');
            
            return false;
        }).filter((hasHash)?'[href="'+hasHash+'"]':':first').click();
    }
})(jQuery);
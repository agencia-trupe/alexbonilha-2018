<?php

class IndexController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        $this->paginas = new Application_Model_Db_PaginaHome();
    }

    public function indexAction()
    {
        $pagina = Is_Array::utf8DbRow(
            $this->paginas->fetchRow('id = 1')
        );

        $pagina->fotos = $this->paginas->getFotos($pagina->id,1,$this->view->lang);
        // _d($pagina->fotos);
        
        $this->view->pagina = $pagina;
    }


}
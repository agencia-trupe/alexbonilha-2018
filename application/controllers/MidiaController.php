<?php

class MidiaController extends ZendPlugin_Controller_Action
{

    public function init()
    {

    }

    public function indexAction()
    {
        // $this->_redirect('midia/clipping');

        $this->clippingAction();
        $this->videosAction();
        $rows = array();
        $this->view->rows = $rows;
        $tipos = array('clippings','videos');

        foreach($tipos as $tipo) foreach($this->view->{$tipo} as $row) {
            $key = (bool)trim($row->data) ? $row->data : reset(explode(' ',$row->data_cad));
            $key = str_replace('-', '', $key);
            $keyi=0; while(array_key_exists($key.$keyi, $rows)) $keyi++;
            if($tipo=='videos'&&$row->id==1) $key='20150128';
            $rows[$key.$keyi] = $row;
            $rows[$key.$keyi]->tipo_row = substr($tipo, 0, -1);
        }

        ksort($rows);
        $rows = array_reverse($rows);
        $this->view->rows = $rows;
        // _d($rows);
    }

    public function clippingAction()
    {
        $this->clippings = new Application_Model_Db_Clippings();

        $limit = 1000;
        $categoria = null;
        $paginacao = $this->pagination($limit,10,$this->clippings->count('status_id = 1'.(($categoria)?' and categoria_id = "'.$categoria.'"':'')));
        // _d($paginacao);
        
        $rows = $this->clippings->getLastProjects(
            $paginacao->offset.','.$limit,
            (($categoria)?'categoria_id = "'.$categoria.'"':null),
            'p.data, p.data_cad'
        );

        if((bool)$rows) $rows = $this->clippings->getFotos($rows);

        $this->view->rows = $rows;
        $this->view->clippings = $rows;
        $this->view->pagination = $paginacao;
        $this->view->categoria = $categoria;
        // _d($this->view->rows);
    }

    public function materiasAction()
    {
        $this->materias = new Application_Model_Db_Materias();

        $limit = 600;
        $paginacao = $this->pagination($limit,10,$this->materias->count('status_id = 1'));
        // _d($paginacao);
        
        $rows = Is_Array::utf8DbResult($this->materias->fetchAll(
            'status_id = 1',
            'data_cad desc',
            $limit, $paginacao->offset
        ));

        $this->view->rows = $rows;
        $this->view->materias = $rows;
        $this->view->pagination = $paginacao;
        // _d($this->view->rows);
    }

    public function videosAction()
    {
        $this->videos = new Application_Model_Db_Videos();

        $limit = 600;
        $paginacao = $this->pagination($limit,10,$this->videos->count('status_id = 1'));
        // _d($paginacao);
        
        $rows = Is_Array::utf8DbResult($this->videos->fetchAll(
            'status_id = 1',
            // 'data_cad desc',
            array('data','data_cad'),
            $limit, $paginacao->offset
        ));

        $this->view->rows = $rows;
        $this->view->videos = $rows;
        $this->view->pagination = $paginacao;
        // _d($this->view->rows);
    }


}


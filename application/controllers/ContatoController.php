<?php

class ContatoController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        /* Initialize action controller here */
        $this->view->meta_description = 'Contato '.SITE_TITLE;
        $this->messenger = new Helper_Messenger();
    }

    public function indexAction()
    {
        $this->view->titulo = 'Contato';
    }
    
    public function enviarAction()
    {
        $r = $this->getRequest();

        if(!$this->isAjax() && !$r->isPost()){
            $this->messenger->addMessage('Requisição inválida','error');
            return $this->_redirect('contato');
        }
        if(!$r->isPost()) return array('error'=>'Requisição inválida');
        
        $form = new Application_Form_Contato();
        $post = $r->getPost();
        $assunto = (bool)trim($r->getParam('assunto')) ? ' - '.trim($r->getParam('assunto')) : '';
        
        if($form->isValid($post)){ // valida post
            $html = "<h1>Contato".$assunto."</h1>". // monta html
                    nl2br($r->getParam('mensagem'))."<br/><br/>".
                    "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                    "<b>E-mail:</b> <a href='mailto:".
                    $r->getParam('email')."'>".$r->getParam('email').
                    "</a><br/>".
                    "<b>Telefone:</b> ".$r->getParam('telefone')."<br/>";
                    // "<b>Celular:</b> ".$r->getParam('celular');
            
            try { // tenta enviar o e-mail
                if(APP_ENV!='production1') Trupe_AlexBonilha_Mail::sendWithReply(
                    $post['email'],
                    $post['nome'],
                    'Contato'.$assunto,
                    $html
                );
                return array('msg'=>$this->view->translate('campo_status_ok'));
            } catch(Exception $e){
                return array('error'=>$e->getMessage());
            }
        }
        
        return array('error'=>'* '.$this->view->translate('campo_status_preencha'));
    }

    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }


}
<?php

class Application_Model_Db_VagasSetores extends ZendPlugin_Db_Table
{
    protected $_name = "vagas_setores";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Vagas');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Vagas' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Vagas',
            'refColumns'    => 'setor_id'
        )
    );
}
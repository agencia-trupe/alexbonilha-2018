<?php

class Application_Model_Db_Fotos extends Zend_Db_Table {
    protected $_name = "fotos";
    
    protected $_dependentTables = array(
        'Application_Model_Db_CasesFotos',
        'Application_Model_Db_PortfolioFotos',
        'Application_Model_Db_ProdutosFotos',
        'Application_Model_Db_PromocoesFotos',
        'Application_Model_Db_DestaquesFotos',
        'Application_Model_Db_BlogsFotos',
        'Application_Model_Db_Categorias',
        'Application_Model_Db_BannerHome',
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_PortfolioFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PortfolioFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_CasesFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_CasesFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_ProdutosFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ProdutosFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_PromocoesFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PromocoesFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_DestaquesFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_DestaquesFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_BlogsFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_BlogsFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_Categorias' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Categorias',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_BannerHome' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_BannerHome',
            'refColumns'    => 'foto_id'
        )
    );
    
    /**
     * Retorna registros com sua tabela correspondente
     *
     * @param string $table  - Nome da tabela de referência junto com seu alias
     *                         Ex.: produto_fotos as pf
     * @param string $where  - Condição where para aplicar à query [optional]
     * @param array  $fields - Campos a serem selecionados da tabela relacionada [optional]
     * @param mixed  $order  - Ordenação do resultado (segue padrões Zend_Table) [optional]
     * @param int    $limit  - Limite de resultados [optional]
     *
     * @return array - Coleção de registros da consulta
     */
    public function fetchJoin($table,$where=null,$fields=array(),$order=null,$limit=null)
    {
        $select = $this->select()->reset();
        $as = strstr($table,'as ') ? end(explode('as ',$table)) : null;
        
        if(!$as) throw new Exception("The table name ($table) don't have an alias.");
        
        $select->from($table,$fields)
               ->joinLeft('fotos as f','f.id = '.$as.'.foto_id',array('*'));
        
        if($where) $select->where($where);
        if($order) $select->order($order);
        if($limit) $select->limit($limit);
        
        $o = $select->query()->fetchAll();
        
        //if(count($o)) $o = array_map('Is_Array::toObject',$o);
        if(count($o)){foreach($o as &$oo){$oo=Is_Array::toObject($oo);}}
	
        return $o;
    }
}

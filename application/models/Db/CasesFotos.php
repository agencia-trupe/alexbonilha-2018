<?php

class Application_Model_Db_CasesFotos extends Zend_Db_Table
{
    protected $_name = "cases_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Cases');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Cases' => array(
            'columns' => 'case_id',
            'refTableClass' => 'Application_Model_Db_Cases',
            'refColumns'    => 'id'
        )
    );
}

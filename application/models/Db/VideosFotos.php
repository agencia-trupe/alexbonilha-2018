<?php

class Application_Model_Db_VideosFotos extends Zend_Db_Table
{
    protected $_name = "videos_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Videos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Videos' => array(
            'columns' => 'video_id',
            'refTableClass' => 'Application_Model_Db_Videos',
            'refColumns'    => 'id'
        )
    );
}

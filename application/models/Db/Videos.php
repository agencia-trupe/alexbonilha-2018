<?php

class Application_Model_Db_Videos extends ZendPlugin_Db_Table 
{
    protected $_name = "videos";
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }

    /**
     * Retorna as fotos do da video
     *
     * @param int $id - id da video
     *
     * @return array - rowset com fotos da video
     */
    public function getFotosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('videos_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('video_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    /**
     * Retorna fotos dos videos
     * 
     * @param array $videos - rowset de videos para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - videos com fotos
     */
    public function getFotos($videos,$withObjects=false)
    {
        $pids = array(); // ids de videos

        // identificando videos
        foreach($videos as $video) $pids[] = $video->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('videos_fotos as fc',array('fc.video_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.video_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            // $fotos = $select->query()->fetchAll();
            // $fotos = count($fotos) ? array_map('Is_Array::toObject',$fotos) : array();
            $_fotos = $select->query()->fetchAll();
            $fotos = array();
            if(count($_fotos)){ foreach($_fotos as $foto){ 
                $fotos[] = Is_Array::toObject($foto);
            }}
        } else {
            $mVideos_fotos = new Application_Model_Db_VideosFotos();
            $fotos = $mVideos_fotos->fetchAll('video_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
        }

        // associando fotos
        foreach($videos as &$video){
            $video->fotos = $this->getFotosSearch($video->id,$fotos);
        }

        return $videos;
    }

    /**
     * Monta rowset de fotos com base no video_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->video_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }

    /**
     * Retorna registros anterior e próximo para paginação
     */
    public function getPrevNext($value,$field='id')
    {
        $rows = array();

        $rows['prev'] = Is_Array::utf8DbRow($this->fetchRow($field.' < "'.$value.'"',$field.' desc',1));
        $rows['next'] = Is_Array::utf8DbRow($this->fetchRow($field.' > "'.$value.'"',$field.' asc',1));

        return $rows;
    }
    
}
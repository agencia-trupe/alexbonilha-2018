<?php

class Application_Model_Db_Cases extends ZendPlugin_Db_Table 
{
    protected $_name = "cases";
    
    /**
     * Referências
     */
    protected $_dependentTables = array(
        'Application_Model_Db_Categorias',
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_Categorias' => array(
            'columns' => 'categoria_id',
            'refTableClass' => 'Application_Model_Db_Categorias',
            'refColumns'    => 'id'
        ),
    );
    
    /**
     * Retorna produto com suas imagens com base no alias se @alias for string ou id se @alias for numérico
     *
     * @param string|int $alias - valor do alias ou id do produto
     *
     * @return object|bool - objeto contendo o produto com suas imagens e categoria ou false se não for encontrado
     */
    public function getWithFotos($alias)
    {
        $column = is_numeric($alias) ? 'id' : 'alias';
        if(!$produto = $this->fetchRow($column.'="'.$alias.'"')){
            return false;
        }
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_ProdutosFotos')){
            foreach($produto_fotos as $produto_foto){
                $fotos[] = $produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current();
            }
        }
        
        $object = Is_Array::utf8DbRow($produto);
        $object->categoria = Is_Array::utf8DbRow($produto->findDependentRowset('Application_Model_Db_Categorias')->current());
        $object->fotos = $fotos;
        //Is_Var::dump($object);
        return $object;
    }
    
    /**
     * Retorna as fotos do produto
     *
     * @param int $id - id do produto
     *
     * @return array - rowset com fotos do produto
     */
    public function getFotos($id)
    {
        if(!$produto = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_ProdutosFotos')){
            foreach($produto_fotos as $produto_foto){
                $fotos[] = Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }
    
    public function hasChildren($id)
    {
        return (bool) $this->fetchRow('parent_id='.$id);
    }
    
    public function getChildren($id,$order='ordem')
    {
        return $this->fetchAll('parent_id='.$id,$order);
    }
}
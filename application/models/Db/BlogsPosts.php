<?php

class Application_Model_Db_BlogsPosts extends ZendPlugin_Db_Table 
{
    protected $_name = "blogs_posts";
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }

    /**
     * Retorna as fotos da noticia
     *
     * @param int $id - id da noticia
     *
     * @return array - rowset das fotos
     */
    public function getFotos1($id)
    {
        if(!$produto = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_BlogsFotos')){
            foreach($produto_fotos as $produto_foto){
                $fotos[] = Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }

    /**
     * Retorna as fotos da noticia
     *
     * @param int $id - id da noticia ou row noticia
     *
     * @return array - rowset das fotos
     */
    public function getFotos($id)
    {
        if(is_numeric($id)){
            if(!$produto = $this->fetchRow('id="'.$id.'"')){
                return false;
            }
        } else {
            $produto = $id;
        }

        $fotos = $this->q(
            'select f.*, bf.blog_post_id from blogs_fotos bf '.
            'left join fotos f on f.id = bf.foto_id '.
            'where bf.blog_post_id = '.$produto->id
        );
        
        return $fotos;
    }

    /**
     * Retorna as categorias da noticia
     *
     * @param int $id - id da noticia ou row noticia
     *
     * @return array - rowset das categorias
     */
    public function getCategoria($id)
    {
        if(is_numeric($id)){
            if(!$produto = $this->fetchRow('id="'.$id.'"')){
                return false;
            }
        } else {
            $produto = $id;
        }

        $categoria = $this->q(
            'select * from categorias_blog '.
            'where id = '.$produto->categoria_id
        );
        
        return (bool)$categoria ? $categoria[0] : null;
    }

    public function getDestaquePosts($post,$count=10,$where=null,$thumb=0)
    {
        return $this->q(
            'select p.id, p.titulo_pt, p.titulo_en, p.alias, p.data_cad '.
            (($thumb) ? ',f.path foto_path ' : '').
            'from blogs_posts p '.
            (($thumb) ? 
                'left join blogs_fotos bf on bf.blog_post_id = p.id '.
                    'left join fotos f on f.id = bf.foto_id '
                : '').
            'where p.blog_id = 1 and p.status_id = 1 and destaque = 0 '.
            
            ($where!==null ? 'and '.$where.' ' : ' ').
            (($thumb) ? 'group by p.id ' : '').
            'order by p.data_cad desc '.
            'limit '.$count
        );
    }

    public function getRelatedPosts($post,$count=10,$where=null,$thumb=0)
    {
        return $this->q(
            'select p.id, p.titulo_pt, p.titulo_en, p.alias, p.data_cad '.
            (($thumb) ? ',f.path foto_path ' : '').
            'from blogs_posts p '.
            (($thumb) ? 
                'left join blogs_fotos bf on bf.blog_post_id = p.id '.
                    'left join fotos f on f.id = bf.foto_id '
                : '').
            'where p.blog_id = 1 and p.status_id = 1 '.

            // relação
            'and p.categoria_id = '.((int)$post->categoria_id).' '.
            'and p.id not in ('.((int)$post->id).') '.
            
            ($where!==null ? 'and '.$where.' ' : ' ').
            (($thumb) ? 'group by p.id ' : '').
            'order by p.data_cad desc '.
            'limit '.$count
        );
    }

    public function getLastPosts($count=10,$where=null)
    {
        return $this->q(
            'select id, titulo_pt, titulo_en, alias, data_cad from blogs_posts '.
            'where blog_id = 1 and status_id = 1 '.
            ($where!==null ? 'and '.$where.' ' : ' ').
            'order by data_cad desc '.
            'limit '.$count
        );
    }
    
}
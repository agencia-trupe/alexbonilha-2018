<?php

class Application_Model_Db_Vagas extends ZendPlugin_Db_Table
{
    protected $_name = "vagas";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_VagasSetores');
    
    protected $_referenceMap = array(
        'Application_Model_Db_VagasSetores' => array(
            'columns' => 'setor_id',
            'refTableClass' => 'Application_Model_Db_VagasSetores',
            'refColumns'    => 'id'
        )
    );

    public function getVagas($where=null,$order=null,$limit=null,$orderBySetor=true,$utf8=true)
    {
        $vagas_setores = new Application_Model_Db_VagasSetores();
        $vagas = $this->fetchAll($where,$order,$limit);
        if($utf8) $vagas = Is_Array::utf8DbResult($vagas);

        if(count($vagas)){
            // pegando setores
            $setores = array();

            foreach($vagas as &$v){
                if(!isset($setores[$v->setor_id])){
                    $setor = $vagas_setores->fetchRow('id='.$v->setor_id);
                    if($utf8) $setor = Is_Array::utf8DbRow($setor);

                    $setores[$v->setor_id] = $setor;
                }

                $v->setor = $setores[$v->setor_id];
            }
        }
        
        return $vagas;
    }
}
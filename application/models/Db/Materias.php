<?php

class Application_Model_Db_Materias extends ZendPlugin_Db_Table 
{
    protected $_name = "materias";
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }

    /**
     * Retorna as fotos do da materia
     *
     * @param int $id - id da materia
     *
     * @return array - rowset com fotos da materia
     */
    public function getFotosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('materias_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('materia_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    /**
     * Retorna fotos dos materias
     * 
     * @param array $materias - rowset de materias para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - materias com fotos
     */
    public function getFotos($materias,$withObjects=false)
    {
        $pids = array(); // ids de materias

        // identificando materias
        foreach($materias as $materia) $pids[] = $materia->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('materias_fotos as fc',array('fc.materia_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.materia_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            // $fotos = $select->query()->fetchAll();
            // $fotos = count($fotos) ? array_map('Is_Array::toObject',$fotos) : array();
            $_fotos = $select->query()->fetchAll();
            $fotos = array();
            if(count($_fotos)){ foreach($_fotos as $foto){ 
                $fotos[] = Is_Array::toObject($foto);
            }}
        } else {
            $mMaterias_fotos = new Application_Model_Db_MateriasFotos();
            $fotos = $mMaterias_fotos->fetchAll('materia_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
        }

        // associando fotos
        foreach($materias as &$materia){
            $materia->fotos = $this->getFotosSearch($materia->id,$fotos);
        }

        return $materias;
    }

    /**
     * Monta rowset de fotos com base no materia_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->materia_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }

    /**
     * Retorna registros anterior e próximo para paginação
     */
    public function getPrevNext($value,$field='id')
    {
        $rows = array();

        $rows['prev'] = Is_Array::utf8DbRow($this->fetchRow($field.' < "'.$value.'"',$field.' desc',1));
        $rows['next'] = Is_Array::utf8DbRow($this->fetchRow($field.' > "'.$value.'"',$field.' asc',1));

        return $rows;
    }
    
}
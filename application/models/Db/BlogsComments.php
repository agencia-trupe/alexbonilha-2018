<?php

class Application_Model_Db_BlogsComments extends ZendPlugin_Db_Table 
{
    protected $_name = "blogs_comments";
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }

    /**
     * Retorna as fotos da noticia
     *
     * @param int $id - id da noticia
     *
     * @return array - rowset das fotos
     */
    public function getFotos($id)
    {
        if(!$produto = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_DiarioFotos')){
            foreach($produto_fotos as $produto_foto){
                $fotos[] = Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }
    
}
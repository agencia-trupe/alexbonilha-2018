<?php

class Admin_DadosEmpresaController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Admin_Model_Login::checkAuth($this);
        
        $this->view->titulo = "DADOS DA EMPRESA";
        $this->view->section = $this->section = "dados-empresa";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path = $this->view->img_path = APPLICATION_PATH."/../..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../..".FILE_PATH."/".$this->section;
        
        // models
        $this->dados = new Application_Model_Db_DadosEmpresa();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();

        $this->view->MAX_FOTOS = 1;
    }

    public function indexAction()
    {
        // $this->_redirect('admin/dados-empresa/edit/1/');
        $where = '1=1';

        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            $rows = $post['search-action']=="search" ?
                    $this->dados->fetchAll($where.' and '.$post['search-by']." like '%".utf8_decode($post['search-txt'])."%'",'nome') :
                    $this->dados->fetchAll($where,$post['search-by']);
        } else {
            $rows = $this->dados->fetchAll($where,'id');
        }
        
        $this->view->rows = Is_Array::utf8DbResult($rows);
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; NOVO");
        $form = new Admin_Form_DadosEmpresa();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $this->view->id = $this->usuario_id = $data['id'];
            $form->addElement('hidden','id');

            if($data['id']!=1) {
                $form->removeElement('tel4');
                $form->removeElement('facebook');
                $form->removeElement('linkedin');
                $form->removeElement('instagram');
                $form->removeElement('pinterest');
            }

            $this->view->fotos = ((bool)trim($data['foto'])) ? array((object)array('id'=>$data['id'],'foto'=>$data['foto'],'path'=>$data['foto'],'foto_path'=>$data['foto'])) : null;//$this->fotosAction();
            $this->view->allow_photos = 1;//$data['allow_photos'];
            // _d($this->view->fotos);
        } else {
            return $this->_forward('index');
            $data = array('status_id'=>'1');
        }
        
        $form->populate($data);
        $this->view->form = $form;
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url.'new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->dados->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $data = array_map('utf8_decode',$this->_request->getParams());
            $data['tel4'] = (bool)trim($data['tel4']) ? Is_Str::removeCaracteres($data['tel4']) : null;
            $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
            $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
            
            // clean
            $cleans = 'cep';//,tel1,tel2,tel3,tel4';
            foreach(explode(',',$cleans) as $c) $data[$c] = Is_Cpf::clean($data[$c]);

            if(($row && $data['senha'] != '') || !$row){
                $data['senha'] = md5($data['senha']);
            } else {
                unset($data['senha']);
            }
            
            // remove dados desnecessários
            if(isset($data['submit'])){ unset($data['submit']); }
            if(isset($data['module'])){ unset($data['module']); }
            if(isset($data['controller'])){ unset($data['controller']); }
            if(isset($data['action'])){ unset($data['action']); }
            
            ($row) ? $this->dados->update($data,'id='.$id) : $id = $this->dados->insert($data);
            
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>Is_Array::utf8All($data)));
        } catch(Exception $e) {
            $this->messenger->addMessage($e->getMessage(),'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->dados->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('not-found','error','default',array('url'=>$this->_url));return false; }
        $this->_forward('new',null,null,array('data'=>Is_Array::utf8All($row->toArray())));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        
        try {
            $this->dados->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        // $fotos = new Application_Model_Db_Fotos();
        // $foto = $fotos->fetchRow('id='.(int)$id);
        $foto = $this->dados->fetchRow('id='.(int)$id);
                
        try {
            // $fotos->delete("id=".(int)$id);
            $path = $foto->foto;
            $foto->foto = null;
            $foto->save();
            Is_File::del($this->img_path.'/'.$path);
            Is_File::delDerived($this->img_path.'/'.$path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        $max_size = intval(ini_get('post_max_size')).'MB'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/servicos/'));
            return;
        }
        
        $file = $_FILES['file'];
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $noticia_id = $this->_getParam('id');
            
            $data_foto = array(
                "foto"     => $rename,
                "user_edit" => $this->login->user->id,
                "data_edit" => date("Y-m-d H:i:s")
            );
            
            $this->dados->update($data_foto,"id = '".$noticia_id."'");
            
            return array("name"=>$rename,"id"=>$noticia_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}
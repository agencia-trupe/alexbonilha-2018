<?php

class Admin_MenusController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "Menus";
        $this->view->section = $this->section = "menus";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
        
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../..".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../..".FILE_PATH."/".$this->section;
        
        $this->cases = new Application_Model_Db_Cases();
    }

    public function indexAction()
    {
        $items = Is_Array::utf8DbResult($this->cases->fetchAll('parent_id=0 and status_id=1','ordem'),false);
        
        foreach($items as &$item){
            $item->children = Is_Array::utf8DbResult($this->cases->getChildren($item->id),false);
        }
        
        $this->view->rows = $items;
    }
    
    public function saveItemsAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>$this->_url));
            return;
        }
        
        $duplicate_count = 0;
        $duplicates = array();
        $params = $this->_request->getParams();
        
        try {
            for($i=0;$i<sizeof($params['id']);$i++){
                $data = array();
                $row = $this->cases->fetchRow('id='.$params['id'][$i]); // verifica registro para atualização
                
                $data['ordem']     = $params['ordem'][$i];
                $data['user_'.($row?'edit':'cad')] = $this->login->user->id;
                $data['data_'.($row?'edit':'cad')] = date("Y-m-d H:i:s");
                
                if($row){
                    $up = 0;
                    if($row->ordem != $data['ordem']){ $row->ordem = $data['ordem']; $up++; }
                    if($up > 0){ $row->save(); }
                } else {
                    //$this->rodape_items->insert($data);
                }
            }
            
            $this->messenger->addMessage('Registros atualizados.');
            $this->_redirect('admin/menus');
        } catch(Exception $e) {
            $this->messenger->addMessage($e->getMessage(),'error');
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
    }
}


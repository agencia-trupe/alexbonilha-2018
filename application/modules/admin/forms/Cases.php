<?php

class Admin_Form_Cases extends Zend_Form
{
	protected $_case;
	
	/**
	 * Seta id do case inicial para ser usado na montagem do combo
	 */
	public function __construct($case=null,$options = null)
	{
		$this->_case = $case;
		parent::__construct($options);
	}
	
    public function init()
    {
        $this->setMethod('post')->setAction(URL.'/admin/cases/save')->setAttrib('id','frm-pousada')->setAttrib('name','frm-pousada');
		
		$tblCases = new Application_Model_Db_Cases();
		$where = 'parent_id = 0'.($this->_case?' and id <> '.$this->_case:'');
		$cases = $tblCases->getKeyValues('titulo',true,'titulo',$where);
		
		$this->addElement('select','parent_id',array('label'=>'Projeto pai:','class'=>'txt','multiOptions'=>$cases));
		$this->addElement('text','titulo',array('label'=>'Título:','class'=>'txt'));
		$this->addElement('checkbox','status_id',array('label'=>'Ativo:','class'=>'','checked'=>true));
		$this->addElement('textarea','body',array('label'=>'Conteúdo:','class'=>'txt'));
		$this->addElement('hidden','alias',array('class'=>'txt'));
        $this->addElement('textarea','body',array('class'=>'txt wysiwyg'));
        $this->addElement('submit','submit',array('label'=>($this->_case?'Salvar':'Criar'),'class'=>'bt'));
        
        //$this->getElement('status_id')->setRequired();
        $this->getElement('body')->setAttrib('rows','20')->setAttrib('cols','103');
        
        $this->removeDecs(array('label','htmlTag','description','errors'));
    }
    
    public function removeDecs($decorators = array('label','htmlTag','description','errors'),$elms=array())
    {
        $_elms = &$this->getElements();
        $elms = count($elms) ? $elms : $_elms;
        foreach($elms as $elm){
            foreach($decorators as $decorator){
                $elm->removeDecorator($decorator);
            }
        }
        return $this;
    }
}
<?php

class Admin_Form_Portfolio extends ZendPlugin_Form
{
    public function init()
    {
		$section = 'projetos';
		
        // configurações
        $this->setMethod('post')->setAction(URL.'/admin/'.$section.'/save')
             ->setAttrib('id','frm-'.$section.'')
             ->setAttrib('name','frm-'.$section.'');

        $categs = new Application_Model_Db_CategoriasPortfolio();
		
        // elementos
        $this->addElement('text','titulo_pt',array('label'=>'Título (pt)','class'=>'txt'));
        $this->addElement('text','titulo_en',array('label'=>'Título (en)','class'=>'txt'));
        // $this->addElement('text','cliente',array('label'=>'Cliente','class'=>'txt'));
        $this->addElement('select','categoria_id',array('label'=>'Tipo','class'=>'txt','multiOptions'=>$categs->getKeyValues('descricao_pt')));
        $this->addElement('text','local_pt',array('label'=>'Local (pt)','class'=>'txt'));
        $this->addElement('text','local_en',array('label'=>'Local (en)','class'=>'txt'));
        $this->addElement('text','cidade_pt',array('label'=>'Cidade (pt)','class'=>'txt'));
        $this->addElement('text','cidade_en',array('label'=>'Cidade (en)','class'=>'txt'));
        $this->addElement('text','uf_pt',array('label'=>'Estado (pt)','class'=>'txt','maxlength'=>2));
        $this->addElement('text','uf_en',array('label'=>'Estado (en)','class'=>'txt','maxlength'=>2));
        $this->addElement('text','area',array('label'=>'Área','class'=>'txt'));
        $this->addElement('text','ano',array('label'=>'Ano','class'=>'txt mask-year',));
        $this->addElement('textarea','descricao_pt',array('label'=>'Descrição (pt)','class'=>'txt'));
        $this->addElement('textarea','descricao_en',array('label'=>'Descrição (en)','class'=>'txt'));
        // $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        //$this->getElement('descricao')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo_pt')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}


<?php

class Admin_Form_Materias extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/materias/save')
             ->setAttrib('id','frm-materias')
             ->setAttrib('name','frm-materias');
        
        // elementos
        $this->addElement('text','titulo_pt',array('label'=>'Título (pt)','class'=>'txt'));
        $this->addElement('text','titulo_en',array('label'=>'Título (en)','class'=>'txt'));
        $this->addElement('hidden','alias');
        // $this->addElement('text','autor',array('label'=>'Autor','class'=>'txt'));
        // $this->addElement('text','link',array('label'=>'Link','class'=>'txt'));
        // $this->addElement('text','data',array('label'=>'Data','class'=>'txt mask-date'));
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        $this->addElement('textarea','body_pt',array('label'=>'Conteúdo (pt)','class'=>'txt wysiwyg'));
        $this->addElement('textarea','body_en',array('label'=>'Conteúdo (en)','class'=>'txt wysiwyg'));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        
        // atributos
        $this->getElement('body_pt')->setAttrib('rows',15)->setAttrib('cols',1);
        $this->getElement('body_en')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo_pt')->setRequired();
        $this->getElement('titulo_en')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}


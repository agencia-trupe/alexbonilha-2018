<?php

class Admin_Form_Ideacao extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/ideacao/save')
             ->setAttrib('id','frm-home')
             ->setAttrib('name','frm-home');
        
        // elementos
        $this->addElement('text','video_titulo_pt',array('label'=>'Título do Vídeo (pt):','class'=>'txt'));
        $this->addElement('text','video_titulo_en',array('label'=>'Título do Vídeo (en):','class'=>'txt'));
        // $this->addElement('text','video',array('label'=>'URL do Vídeo:','class'=>'txt'));
        $this->addElement('text','video_pt',array('label'=>'URL do Vídeo (pt):','class'=>'txt'));
        $this->addElement('text','video_en',array('label'=>'URL do Vídeo (en):','class'=>'txt'));
        // $this->addElement('text','titulo',array('label'=>'Título:','class'=>'txt'));
        $this->addElement('textarea','texto1_pt',array('label'=>'Texto (pt):','class'=>'txt wysiwyg'));
        $this->addElement('textarea','texto1_en',array('label'=>'Texto (en):','class'=>'txt wysiwyg'));
        // $this->addElement('textarea','texto2',array('label'=>'Texto 2:','class'=>'txt wysiwyg'));
        
        // atributos
        $this->getElement('texto1_pt')->setAttrib('rows',10);
        $this->getElement('texto1_en')->setAttrib('rows',10);
        // $this->getElement('texto2')->setAttrib('rows',10);
        
        // filtros / validações
        // $this->getElement('video')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}
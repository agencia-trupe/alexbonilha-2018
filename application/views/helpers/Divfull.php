<?php
/**
 * Div full width (além dos limites da div.main)
 * 
 */
class Zend_View_Helper_Divfull extends Zend_View_Helper_Abstract
{
    public $section = "";
    
    /**
     * Método Principal
     * @param string $value Valor para Formatação
     */
    public function divfull($section=null)
    {
        $this->section = ($section) ? $section : @$this->view->section;
        return $this;
    }

    public function open($wrapper='')
    {
        $html = '</article></div></div>'.$wrapper.
            '<div class="main main-'.$this->view->controller.' main-full main-full-'.$this->view->controller.'-'.$this->section.' main-full-open main-full-open-'.$this->section.'">'.
            '<div class="main-full-container main-full-container-'.$this->section.'">'.
            '<div id="content" class="content content-'.$this->section.'">'.
            '<article>';

        return $html;
    }

    public function close($wrapper_close='')
    {
        $html = $wrapper_close.'</article></div></div></div>'.
            '<div class="main main-'.$this->view->controller.' main-full-close main-full-close-'.$this->section.'">'.
            '<div id="content" class="content content-'.$this->view->controller.' content-'.$this->view->controller.'-'.$this->view->action.'">'.
            '<article>';

        return $html;
    }

}
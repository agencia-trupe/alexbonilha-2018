<?php
/**
 * Tradução de conteúdo vindo do banco de dados
 * Auxiliar da Camada de Visualização
 * @author Patrick M. de Olveira - Trupe Agência Criativa
 * @see APPLICATION_PATH/views/helpers/TranslateDb.php
 */

class Zend_View_Helper_TranslateDb extends Zend_View_Helper_Abstract
{
    /**
     * Método Principal
     *
     * @param object $object   - Objeto base para extrair valores
     * @param string $property - propriedade a ser traduzida
     *
     * @return string - Texto traduzido
     */
    public function translateDb($object,$property,$firstAvailable=false)
    {
        $translate = null;

        if((bool)trim($object->{$property.'_'.$this->view->lang})){
            $translate = $object->{$property.'_'.$this->view->lang};
        } else if((bool)trim($object->{$property.'_'.DEFAULT_LANGUAGE})){
            $translate = $object->{$property.'_'.DEFAULT_LANGUAGE};
        }

        if($firstAvailable && !(bool)$translate){
            foreach (get_object_vars($object) as $key => $value) {
                if(strstr($key,$property.'_') && (bool)trim($value)) {
                    $translate = $value;
                    break;
                }
            }
        }

        return $translate;
    }
}
<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{    
    protected function _initRoute()
    {
        $this->router = new Zend_Controller_Router_Rewrite();
		$this->request =  new Zend_Controller_Request_Http();
		$this->router->route($this->request); // pegar todos os parametros
        
        $this->bootstrap('view');
        $this->view = $this->getResource('view');
        $this->bootstrap('layout');
        $this->layout = $this->getResource('layout');
        
        if($this->request->getModuleName()=="default"){
            // dados da empresa
            $dados_empresa = new Application_Model_Db_DadosEmpresa();
            $dados = Is_Array::utf8DbRows(
                $dados_empresa->fetchAll(null,'id')
            );
            $this->view->dados_empresa = $dados[0];
            $this->view->dados = $this->view->dados_empresa;
            $this->view->dados_empresa2 = $dados[1];
            $this->view->dados2 = $this->view->dados_empresa2;
        }
        
        if($this->request->getControllerName()=="admin"){
            $this->layout->setLayout("admin");
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME));
            
            $this->view->isLogged = $this->view->logged = $auth->hasIdentity();
            $this->view->login = $auth->hasIdentity() ? $auth->getIdentity() : null;
        }
        
        if(array_key_exists('busca',$this->request->getParams())){
            $this->view->busca = $this->request->getParam('busca');
        }

        // configura��es
        $this->site_config = new Zend_Session_Namespace(SITE_NAME.'_site_config');
        if(!isset($this->site_config->lang))   $this->site_config->lang   = DEFAULT_LANGUAGE;

        // translate
        if(isset($_GET['lang'])){
            $lang = trim(addslashes($_GET['lang']));

            if((bool)$lang && in_array($lang,explode(',',SITE_LANGUAGES))){
            	$this->site_config->lang = $lang;
            } else {
            	$this->site_config->lang = DEFAULT_LANGUAGE;
            }
        }

        $locale = new Zend_Locale($this->site_config->lang);
        Zend_Registry::set('Zend_Locale', $locale);
        
        $translate = new Zend_Translate(
            array(
                'adapter' => 'csv',
                'content' => APPLICATION_PATH.'/languages',
                // 'locale'  => 'auto',
                'scan' => Zend_Translate::LOCALE_FILENAME
            )
        );
        $translate->setLocale($locale);
        if (!$translate->isAvailable($locale->getLanguage())) {
            $translate->setLocale(DEFAULT_LANGUAGE);
        }
        // _d($locale->toString().' - '.$locale->getLanguage());
        
        Zend_Registry::set('Zend_Translate', $translate);
        Zend_Form::setDefaultTranslator($translate);

        $this->view->translate_object = $translate;
        $this->view->site_config = $this->site_config;
        $this->view->lang = $this->site_config->lang;
        $this->lang = $this->site_config->lang;
        // _d(array('t'=>$translate->getLocale(),'c'=>$this->site_config));
    }
    
    protected function _initLayoutConfigs(){
        $this->view->doctype('XHTML1_STRICT');
		//$this->view->action = $this->request->getActionName();
		//$this->view->controller = $this->request->getControllerName();
		//$this->view->module = $this->request->getModuleName();
		
		switch($this->request->getControllerName()){
			case 'admin':
				$actionName = $this->request->getActionName();
				$actions = explode('/',$this->request->getRequestUri());
				$curAction = end($actions);
				$s = array_search($actionName,$actions);
				$action = $curAction!=$actions[$s]?$curAction:'index';
				//_d($action);
				$this->view->action = $action;
				$this->view->controller = $actionName;
				$this->view->module = $this->request->getControllerName();
				break;
			default:
				$this->view->action = $this->request->getActionName();
				$this->view->controller = $this->request->getControllerName();
				$this->view->module = $this->request->getModuleName();
		}
    }
    
    /**
	 * used for handling top-level navigation
	 * @return Zend_Navigation
	 */
	protected function _initNavigation()
	{
        if($this->view->controller=='img') return;
        
        if($this->request->getControllerName()=="admin"){            
            $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation-admin.xml', 'nav');
            $config = $config->toArray();
            //Is_Var::dump($config);
            $this->view->menu = new stdClass();
            self::configUrlPrefix($config['admin-top']);
            $this->view->menu->admin_top  = new Zend_Navigation($config['admin-top']);
        } else {
            $table = new Application_Model_Db_BlogsPosts();

            $this->view->blog_last_posts = $table->getLastPosts(3);

        	if($this->request->getControllerName()=="blog") {
	            $config_categ = array();
	            $categs_rows = $table->q(
	            	'select c.*, count(p.id) as post_count from categorias_blog c '.
					'left join blogs_posts p on p.categoria_id = c.id '.
					'where c.status_id = 1 '.
					'group by c.id '.
					'order by c.ordem '
				);
	            // $categs_rows = $categs->fetchAll('status_id = 1','ordem');
	            $i = 0;
	            
	            foreach($categs_rows as $categ){
	                $config_categ[$categ->alias] = array(
	                    'label' => ($categ->{'descricao_'.$this->lang}),
	                    'title' => ($categ->{'descricao_'.$this->lang}),
	                    //'uri'   => (APPLICATION_ENV == 'development'?'/'.SITE_NAME.'/':'/').'categoria/'.$categ->alias.'/'
	                    'id'    => 'categoria-'.$categ->id,
	                    'uri'   => URL.'/blog/categoria/'.$categ->alias,
	                    'alias' => $categ->alias,
	                    'count' => $categ->post_count,
	                    'pages' => array(
	                        array(
	                            'label' => ($categ->{'descricao_'.$this->lang}),
	                            'uri'   => URL.'/blog/categoria/'.$categ->alias,
	                            'alias' => $categ->alias,
	                            'count' => $categ->post_count,
	                            'class' => 'submenu-title'
	                        )
	                    )
	                );
	            }

	            self::configUrlPrefix($config_categ);
	            $config_categ_foot = $config_categ;
	            foreach($config_categ_foot as &$categ){
	                unset($categ['pages']);
	            }
	            // _d($config_categ);

	            $this->view->menu_blog = new stdClass();
	            // $this->view->menu_blog->categorias_top = new Zend_Navigation(array_reverse($config_categ));
	            $this->view->menu_blog->categorias  = new Zend_Navigation($config_categ);
	            // $this->view->menu_blog->categorias_footer  = new Zend_Navigation($config_categ_foot);

	            // $_menu_nav = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
	            // $menu_nav = $_menu_nav->toArray();
	            // $this->view->menu->footer         = new Zend_Navigation($menu_nav['footer']);
        	}
            $_menu_nav = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation-'.$this->lang.'.xml', 'nav');
            $menu_nav = $_menu_nav->toArray();

            // tipos de projeto
            $tipos = $table->s('categorias_portfolio','*',null,'ordem');
            $menu_nav['top']['projetos']['class'] = 'has-sub';
            $menu_nav['top']['projetos']['pages'] = array();
            foreach ($tipos as $t) $menu_nav['top']['projetos']['pages'][] = array(
                'label' => $t->{'descricao_'.$this->lang},
                'uri' => '/projetos/'.$t->alias,
            );
            // _d($menu_nav['top']);

            $menu_nav['top']['whatsapp'] = array(
                'label' => 'whatsapp',
                'class' => 'whatsapp',
                'target' => '_blank',
                'uri' => 'http://api.whatsapp.com/send?phone=55'.$this->view->dados->tel4,
            );
            $menu_nav['footer'] = $menu_nav['top'];
            unset($menu_nav['footer']['contato']);
            unset($menu_nav['footer']['whatsapp']);
            unset($menu_nav['footer']['projetos']['pages']);
			
            self::configUrlPrefix($menu_nav['top']);
			self::configUrlPrefix($menu_nav['footer']);
            $this->view->menu = new stdClass();
            $this->view->menu->top = new Zend_Navigation($menu_nav['top']);
            $this->view->menu->footer = new Zend_Navigation($menu_nav['footer']);
            
            $uri = APPLICATION_ENV == 'development' ?
                    URL.$this->request->getPathInfo() : // dev
                    URL.$this->request->getPathInfo();  // production
            
            foreach(get_object_vars($this->view->menu) as $menu){
                $activeNav = $menu->findByUri($uri) or
                $activeNav = $menu->findByUri(str_replace('http://'.$_SERVER['HTTP_HOST'],'',$uri));
                
                if(null !== $activeNav){
                    $activeNav->active = true;
                    $activeNav->setClass($activeNav->getClass()." active");	
                }
            }
        }
	}

	function configUrlPrefix(&$config)
    {
        foreach($config as &$c){
            // adiciona url ao link
            if(isset($c['uri'])) if($c['uri'] != '#' & !strstr($c['uri'],'http')) $c['uri'] = URL.$c['uri'];
            // adiciona recurs�o � fun��o
            if(isset($c['pages'])) $c['pages'] = self::configUrlPrefix($c['pages']);
        }
        
        return $config;
    }
}